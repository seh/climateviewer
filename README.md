# Climate Viewer 3D
Geographic Information System (GIS) + Activism = Maptivism 

[LAUNCH DEMO](http://climateviewer.net/)

![Climate Viewer](http://climateviewer.com/images/climate-viewer-cesium-beta.jpg)

Climate Viewer 3D empowers you with cutting-edge technology, real-time situational awareness, and a visual tour of our planetary problems.  With a plethora of controls, data sources, [Cesium](http://cesiumjs.org/) powered interface, and fresh content daily, what are you waiting for?

This web application is an open-source alternative to our now-dead [Google Earth powered app](http://climateviewer.com/3D/).  Read all about [our conversion from Google Earth to Cesium](http://climateviewer.com/2014/12/28/death-google-earth-climate-viewer-3d/) on [Climate Viewer News](http://climateviewer.com/) then [follow us on Facebook](https://www.facebook.com/climateviewers).

Contact [Jim Lee](http://climateviewer.com/rezn8d/) to contribute.
